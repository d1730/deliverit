package com.telericacademy.web.deliverit.controllers.mvc;

import com.telericacademy.web.deliverit.controllers.rest.AuthenticationHelper;
import com.telericacademy.web.deliverit.exceptions.AuthenticationFailureException;
import com.telericacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telericacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telericacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telericacademy.web.deliverit.mappers.ShipmentMapper;
import com.telericacademy.web.deliverit.models.*;
import com.telericacademy.web.deliverit.models.dto.ShipmentDto;
import com.telericacademy.web.deliverit.services.contracts.ShipmentService;
import com.telericacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/shipments")
public class ShipmentMvcController {

    private final ShipmentService shipmentService;
    private final ShipmentMapper shipmentMapper;
    private final WarehouseService warehouseService;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public ShipmentMvcController(ShipmentService shipmentService, ShipmentMapper shipmentMapper,
                                 WarehouseService warehouseService, AuthenticationHelper authenticationHelper) {
        this.shipmentService = shipmentService;
        this.shipmentMapper = shipmentMapper;
        this.warehouseService = warehouseService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = authenticationHelper.tryGetUser(session);
            return user.isEmployee();
        }
        return false;
    }

    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        return warehouseService.getAll();
    }

    @GetMapping
    public String showAllShipments(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        model.addAttribute("shipments", shipmentService.getAll(user));
        return "shipments";
    }

    @GetMapping("/{id}")
    public String showSingleShipment(@PathVariable int id, Model model,
                                     HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        try {
            Shipment shipment = shipmentService.getById(id, user);
            model.addAttribute("shipment", shipment);
            return "shipment";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewShipmentPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:login";
        }

        model.addAttribute("shipment", new ShipmentDto());
        return "shipment-new";
    }

    @PostMapping("/new")
    public String createShipment(@Valid @ModelAttribute("shipment") ShipmentDto shipmentDto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "shipment-new";
        }

        try {
            Shipment shipment = shipmentMapper.fromDto(shipmentDto);
            shipmentService.create(shipment, user);

            return "redirect:/shipments";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_shipment", e.getMessage());
            return "shipment-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditShipmentPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            Shipment shipment = shipmentService.getById(id, user);
            ShipmentDto shipmentDto = shipmentMapper.shipmentToDto(shipment);
            model.addAttribute("shipmentId", id);
            model.addAttribute("shipment", shipmentDto);
            return "shipment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateShipment(@PathVariable int id,
                               @Valid @ModelAttribute("shipment") ShipmentDto dto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "shipment-update";
        }

        try {
            Shipment shipment = shipmentMapper.fromDto(dto, id);
            shipmentService.update(shipment, user);
            return "redirect:/shipments";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("shipmentId", "duplicate-shipment", e.getMessage());
            return "shipment-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteShipment(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            shipmentService.delete(id, user);

            return "redirect:/shipments";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

}