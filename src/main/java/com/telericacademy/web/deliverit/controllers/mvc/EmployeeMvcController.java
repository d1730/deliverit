package com.telericacademy.web.deliverit.controllers.mvc;

import com.telericacademy.web.deliverit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/employee")
public class EmployeeMvcController {
    private final UserService userService;

    @Autowired
    public EmployeeMvcController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping
    public String showEmployeePage() {
        return "employee";
    }
}
