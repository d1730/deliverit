package com.telericacademy.web.deliverit.controllers.mvc;

import com.telericacademy.web.deliverit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customer")
public class CustomerMvcController {
    private final UserService userService;

    @Autowired
    public CustomerMvcController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping
    public String homePage() {
        return "customer";
    }
}
