package com.telericacademy.web.deliverit.controllers.mvc;

import com.telericacademy.web.deliverit.controllers.rest.AuthenticationHelper;
import com.telericacademy.web.deliverit.models.User;
import com.telericacademy.web.deliverit.services.contracts.ShipmentService;
import com.telericacademy.web.deliverit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller()
@RequestMapping("/")
public class HomeMvcController {
    private final UserService userService;
    private final ShipmentService shipService;


    @Autowired
    public HomeMvcController(UserService userService, ShipmentService shipService) {
        this.userService = userService;
        this.shipService = shipService;

    }

    //
//    @ModelAttribute("isAuthenticated")
//    public boolean populateIsAuthenticated(HttpSession session) {
//        return session.getAttribute("currentUser") != null;
//    }
    @GetMapping
    public String showCustomer(Model model) {
        model.addAttribute("count", userService.countCustomers());

        return "index";
    }
}