package com.telericacademy.web.deliverit.controllers.mvc;

import com.telericacademy.web.deliverit.controllers.rest.AuthenticationHelper;
import com.telericacademy.web.deliverit.exceptions.AuthenticationFailureException;
import com.telericacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telericacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telericacademy.web.deliverit.mappers.UserMapper;
import com.telericacademy.web.deliverit.models.Parcel;
import com.telericacademy.web.deliverit.models.Status;
import com.telericacademy.web.deliverit.models.User;
import com.telericacademy.web.deliverit.services.contracts.ParcelService;
import com.telericacademy.web.deliverit.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class UserMvcController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final ParcelService parcelService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(UserService userService, UserMapper userMapper, ParcelService parcelService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.parcelService = parcelService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping("/customer-parcels")
    public String showAllCustomerParcels(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        User loggedUser = user;
        model.addAttribute("parcels-all", userService.getUserParcels(user, loggedUser));
        return "parcels-customer-all";
    }

    @GetMapping("/customer-preparing")
    public String showAllCustomerParcelsPreparing(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        User loggedUser = user;
        model.addAttribute("parcels-preparing", userService.getUserParcelsPreparing(user, loggedUser));
        return "parcels-customer-preparing";
    }

    @GetMapping("/customer-incoming")
    public String showAllCustomerParcelsIncoming(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        User loggedUser = user;
        model.addAttribute("parcels-incoming", userService.getUserParcelsIncoming(user, loggedUser));
        return "parcels-customer-incoming";
    }

    @GetMapping("/customer-delivered")
    public String showAllCustomerParcelsDelivered(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        User loggedUser = user;
        model.addAttribute("parcels-delivered", userService.getUserParcelsDelivered(user, loggedUser));
        return "parcels-customer-delivered";
    }

    @GetMapping("status/{parcelId}")
    public String getParcelStatus(Model model, HttpSession session, @PathVariable int parcelId) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        try {
            User loggedUser = user;
            model.addAttribute("parcels-delivered", userService.getUserParcelsDelivered(user, loggedUser));
            return "parcels-customer-status";
        } catch (EntityNotFoundException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


//    @PutMapping("status/{parcelId}")
//    public Parcel setParcelDeliverToUser(@RequestHeader HttpHeaders headers, @PathVariable int parcelId,
//                                         @RequestParam boolean deliverToUser) {
//        try {
//            User loggedUser = authenticationHelper.tryGetUser(headers);
//            return parcelService.setParcelDeliveryToUser(loggedUser, parcelId, deliverToUser);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }
//

}


