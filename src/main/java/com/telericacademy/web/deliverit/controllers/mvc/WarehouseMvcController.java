package com.telericacademy.web.deliverit.controllers.mvc;

import com.telericacademy.web.deliverit.controllers.rest.AuthenticationHelper;
import com.telericacademy.web.deliverit.exceptions.AuthenticationFailureException;
import com.telericacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telericacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telericacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telericacademy.web.deliverit.mappers.WarehouseMapper;
import com.telericacademy.web.deliverit.models.Address;
import com.telericacademy.web.deliverit.models.User;
import com.telericacademy.web.deliverit.models.Warehouse;
import com.telericacademy.web.deliverit.models.dto.WarehouseDto;
import com.telericacademy.web.deliverit.services.contracts.AddressService;
import com.telericacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/warehouses")
public class WarehouseMvcController {


    private final WarehouseService warehouseService;
    private final WarehouseMapper warehouseMapper;
    private final AddressService addressService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WarehouseMvcController(WarehouseService warehouseService, WarehouseMapper warehouseMapper,
                                  AddressService addressService,
                                  AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.warehouseMapper = warehouseMapper;
        this.addressService = addressService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = authenticationHelper.tryGetUser(session);
            return user.isEmployee();
        }
        return false;
    }

    @ModelAttribute("addresses")
    public List<Address> populateAddresses() {
        return addressService.getAll();
    }

    @GetMapping
    public String showAllWarehouses(Model model) {
        model.addAttribute("warehouses", warehouseService.getAll());
        return "warehouses";
    }

    @GetMapping("/{id}")
    public String showSingleWarehouse(@PathVariable int id, Model model) {
        try {
            Warehouse warehouse = warehouseService.getById(id);
            model.addAttribute("warehouse", warehouse);
            return "warehouse";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewWarehousePage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        model.addAttribute("warehouse", new WarehouseDto());
        return "warehouse-new";
    }

    @PostMapping("/new")
    public String createWarehouse(@Valid @ModelAttribute("warehouse") WarehouseDto warehouseDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "warehouse-new";
        }

        try {
            Warehouse warehouse = warehouseMapper.fromDto(warehouseDto);
            warehouseService.create(warehouse, user);

            return "redirect:/warehouses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_warehouse", e.getMessage());
            return "warehouse-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditWarehousePage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            Warehouse warehouse = warehouseService.getById(id);
            WarehouseDto warehouseDto = warehouseMapper.toDto(warehouse);
            model.addAttribute("warehouseId", id);
            model.addAttribute("warehouse", warehouseDto);
            return "warehouse-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateWarehouse(@PathVariable int id,
                             @Valid @ModelAttribute("warehouse") WarehouseDto dto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "warehouse-update";
        }

        try {
            Warehouse warehouse = warehouseMapper.fromDto(dto, id);
            warehouseService.update(warehouse, user);
            return "redirect:/warehouses";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("warehouseId", "duplicate-warehouse", e.getMessage());
            return "warehouse-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteWarehouse(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            warehouseService.delete(id, user);

            return "redirect:/warehouses";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

}



