package com.telericacademy.web.deliverit.controllers.mvc;

import com.telericacademy.web.deliverit.controllers.rest.AuthenticationHelper;
import com.telericacademy.web.deliverit.exceptions.AuthenticationFailureException;
import com.telericacademy.web.deliverit.exceptions.DuplicateEntityException;
import com.telericacademy.web.deliverit.exceptions.EntityNotFoundException;
import com.telericacademy.web.deliverit.exceptions.UnauthorizedOperationException;
import com.telericacademy.web.deliverit.mappers.ParcelMapper;
import com.telericacademy.web.deliverit.models.*;
import com.telericacademy.web.deliverit.models.dto.ParcelDto;
import com.telericacademy.web.deliverit.services.contracts.CategoryService;
import com.telericacademy.web.deliverit.services.contracts.ParcelService;
import com.telericacademy.web.deliverit.services.contracts.UserService;
import com.telericacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/parcels")
public class ParcelMvcController {

    private final ParcelService parcelService;
    private final UserService userService;
    private final WarehouseService warehouseService;
    private final CategoryService categoryService;
    private final ParcelMapper parcelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ParcelMvcController(ParcelService parcelService, UserService userService,
                               WarehouseService warehouseService, CategoryService categoryService,
                               ParcelMapper parcelMapper, AuthenticationHelper authenticationHelper) {
        this.parcelService = parcelService;
        this.userService = userService;
        this.warehouseService = warehouseService;
        this.categoryService = categoryService;
        this.parcelMapper = parcelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isEmployee")
    public boolean populateIsEmployee(HttpSession session) {
        if (session.getAttribute("currentUser") != null) {
            User user = authenticationHelper.tryGetUser(session);
            return user.isEmployee();
        }
        return false;
    }

//@ModelAttribute("users")
//public List<User> populateUsers(){
//        return  userService.getAllCustomers(user)
//}
    @ModelAttribute("warehouses")
    public List<Warehouse> populateWarehouses() {
        return warehouseService.getAll();
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }



    @GetMapping
    public String showAllParcels(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
        model.addAttribute("parcels", parcelService.getAll(user));
        return "parcels";
    }

    @GetMapping("/{id}")
    public String showSingleParcel(@PathVariable int id, Model model,
                                   HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            Parcel parcel = parcelService.getById(id, user);
            model.addAttribute("parcel", parcel);
            return "parcel";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/new")
    public String showNewParcelPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:login";
        }

        model.addAttribute("parcel", new ParcelDto());
        return "parcel-new";
    }

    @PostMapping("/new")
    public String createParcel(@Valid @ModelAttribute("parcel") ParcelDto parcelDto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "parcel-new";
        }

        try {
            Parcel parcel = parcelMapper.fromDto(parcelDto);
            parcelService.create(parcel, user);

            return "redirect:/parcels";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_parcel", e.getMessage());
            return "parcel-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditParcelPage(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        try {
            Parcel parcel = parcelService.getById(id, user);
            ParcelDto parcelDto = parcelMapper.parcelToDto(parcel);
            model.addAttribute("parcelId", id);
            model.addAttribute("parcel", parcelDto);
            return "parcel-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updateParcel(@PathVariable int id,
                               @Valid @ModelAttribute("parcel") ParcelDto dto,
                               BindingResult errors,
                               Model model,
                               HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }

        if (errors.hasErrors()) {
            return "parcel-update";
        }

        try {
            Parcel parcel = parcelMapper.fromDto(dto, id);
            parcelService.update(parcel, user);
            return "redirect:/parcels";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("parcelId", "duplicate-parcel", e.getMessage());
            return "parcel-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteParcel(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            parcelService.delete(id, user);

            return "redirect:/parcels";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

}
