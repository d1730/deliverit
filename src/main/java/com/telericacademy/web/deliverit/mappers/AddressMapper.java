package com.telericacademy.web.deliverit.mappers;

import com.telericacademy.web.deliverit.exceptions.InvalidUserInputException;
import com.telericacademy.web.deliverit.models.Address;
import com.telericacademy.web.deliverit.models.City;
import com.telericacademy.web.deliverit.models.Country;
import com.telericacademy.web.deliverit.models.dto.RegisterDto;
import com.telericacademy.web.deliverit.models.dto.UserInDto;
import com.telericacademy.web.deliverit.services.contracts.AddressService;
import com.telericacademy.web.deliverit.services.contracts.CityService;
import com.telericacademy.web.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    private final AddressService addressService;
    private final CityService cityService;
    private final CountryService countryService;

    @Autowired
    public AddressMapper(AddressService addressService,
                         CityService cityService, CountryService countryService) {
        this.addressService = addressService;
        this.cityService = cityService;
        this.countryService = countryService;
    }

    public Address createFromUser(UserInDto userInDto) {
        Address address = new Address();
        address.setStreetName(userInDto.getStreetName());
        address.setCity(cityService.getById(userInDto.getCityId()));
        addressService.createFromUser(address);
        return address;
    }

    public Address createFromRegister(RegisterDto registerDto) {
        Address address = new Address();
        Country inputCountry = countryService.getById(registerDto.getCountryId());
        City city = cityService.getById(registerDto.getCityId());
        Country countryByCity = city.getCountry();
        if (!countryByCity.equals(inputCountry)) {
            throw new InvalidUserInputException("The city must be from the selected country");
        }
        address.setStreetName(registerDto.getStreetName());
        address.setCity(city);
        addressService.createFromUser(address);
        return address;
    }

}
