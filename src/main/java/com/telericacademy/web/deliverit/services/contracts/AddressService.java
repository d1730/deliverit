package com.telericacademy.web.deliverit.services.contracts;

import com.telericacademy.web.deliverit.models.Address;
import com.telericacademy.web.deliverit.models.dto.UserInDto;

import java.util.List;

public interface AddressService {

    List<Address> getAll();

    Address getById(int id);

    void createFromUser(Address address);


}
