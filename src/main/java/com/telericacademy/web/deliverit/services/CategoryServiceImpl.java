package com.telericacademy.web.deliverit.services;

import com.telericacademy.web.deliverit.models.Category;
import com.telericacademy.web.deliverit.repositories.contracts.CategoryRepository;
import com.telericacademy.web.deliverit.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }
}
