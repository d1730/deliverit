package com.telericacademy.web.deliverit.services.contracts;

import com.telericacademy.web.deliverit.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);


}
