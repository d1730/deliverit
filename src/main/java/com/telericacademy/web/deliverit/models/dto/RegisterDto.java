package com.telericacademy.web.deliverit.models.dto;

import com.telericacademy.web.deliverit.models.City;
import com.telericacademy.web.deliverit.models.Country;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class RegisterDto extends LoginDto {

    @NotEmpty(message = "Password can`t be empty")
    private String passwordConfirm;

    @NotEmpty(message = "FirstName can`t be empty")
    @Size(min = 2, max = 20, message = "First name should be between 2 and 20 symbols.")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20 symbols.")
    private String lastName;

    @NotEmpty(message = "Street name can't be empty")
    @Size(min = 2, max = 50, message = "Street name should be between 2 and 20 symbols.")
    private String streetName;

    @PositiveOrZero(message = "cityId should be positive or zero")
    private int cityId;

    @PositiveOrZero(message = "countryId should be positive or zero")
    private int countryId;

    public RegisterDto() {
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}

