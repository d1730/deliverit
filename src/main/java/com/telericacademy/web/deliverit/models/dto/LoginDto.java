package com.telericacademy.web.deliverit.models.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class LoginDto {

    @NotEmpty
    @Size(min = 6, max = 50, message = "Email should be between 10 and 50 symbols.")
    private String email;

    @NotEmpty
    @Size(min = 2, max = 20, message = "Password should be between 2 and 20 symbols.")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
