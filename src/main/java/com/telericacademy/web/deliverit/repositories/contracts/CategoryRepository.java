package com.telericacademy.web.deliverit.repositories.contracts;

import com.telericacademy.web.deliverit.models.Category;

import java.util.List;

public interface CategoryRepository {

    List<Category> getAll();

    Category getById (int id);
}
