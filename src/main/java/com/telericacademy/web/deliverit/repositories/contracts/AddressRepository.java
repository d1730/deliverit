package com.telericacademy.web.deliverit.repositories.contracts;

import com.telericacademy.web.deliverit.models.Address;

import java.util.List;

public interface AddressRepository {

    List<Address> getAll();

    Address getById (int id);

    void create (Address address);
}
